const HAMBURGER = {
    small: {
        price: 5,
        calories: 20,
    },
    big: {
        price: 10,
        calories: 40,
    },
    toppings: {
        cheese: {
            price: 1,
            calories: 20,
        },
        salad: {
            price: 2,
            calories: 5,
        },
        potato: {
            price: 1.5,
            calories: 10,
        },
        spice: {
            price: 1.5,
            calories: 0,
        },
        mayo: {
            price: 2,
            calories: 5,
        },
    },
};

class Hamburger {
    constructor(size) {
        this.size = size;
        this.toppings = [];
        this.price = this.size.price;
        this.calories = this.size.calories;
    }

    getPrice() {
        return this.price + this.toppings.reduce((acc, el) => acc + el.price, 0);
    }

    getCalories() {
        return this.calories + this.toppings.reduce((acc, el) => acc + el.calories, 0);
    }

    addTopping(topping) {
        this.toppings.push(topping);
    }
}

const humberger = new Hamburger(HAMBURGER.big);

humberger.addTopping(HAMBURGER.toppings.cheese);

console.log(`Hamburger price ${humberger.getPrice()}$`);
console.log(`Hamburger calories ${humberger.getCalories()}`);
